/**
 * Created by dipiash on 06.09.2016.
 */

$(document).ready(function () {

    //======================================================================= Controls =================================
    var $playfield = $('#playfield');
    var $score = $('.score-point');
    var $btnNewGame = $('#new-game');
    var $messageContainer = $('#message');

    var mousedown = false;

    var cursorPosition = {
            x: 0,
            y: 0
        },
        newCursorPosition = {
            x: 0,
            y: 0
        };

    $playfield.unbind('mousedown').mousedown(function (e) {
        mousedown = true;

        cursorPosition.x = e.pageX;
        cursorPosition.y = e.pageY;
    });

    $playfield.unbind('mouseup').mouseup(function (e) {
        mousedown = false;
        $(this).unbind('mouseleave');

        mousedown = false;
        var clearance = 50;

        newCursorPosition.x = e.pageX;
        newCursorPosition.y = e.pageY;

        if (newCursorPosition.x - cursorPosition.x >= clearance) {
            main.eventsUDLF('right');
        } else if (newCursorPosition.x - cursorPosition.x <= -clearance) {
            main.eventsUDLF('left')
        } else if (newCursorPosition.y - cursorPosition.y >= clearance) {
            main.eventsUDLF('down')
        } else if (newCursorPosition.y - cursorPosition.y <= -clearance) {
            main.eventsUDLF('up');
        }
    });

    //======================================================================= Game =====================================

    // Structure for Game
    function MainGame() {
        this.size = 4;

        this.grid = [
            [2, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ];

        this.chance = {
            min: 10,
            max: 90
        };

        this.fieldsForTile = this.fieldsToMatrix();

        this.score = 0;
        this.win = false;

        this.lose = {
            up: 0,
            rigth: 0,
            down: 0,
            left: 0
        };

        this.block = false;
    }

    // Main point - setup game
    MainGame.prototype.setupGame = function (animateTo) {
        var fieldsForTile = this.fieldsForTile;

        var animateToVal = '',
            toAnim = '';
        switch (animateTo) {
            case 'top':
                animateToVal = {"top": '0px'};
                toAnim = 'top';
                break;
            case 'right':
                animateToVal = {"left": '300px'};
                toAnim = 'right';
                break;
            case 'bottom':
                animateToVal = {"top": '300px'};
                toAnim = 'bottom';
                break;
            case 'left':
                animateToVal = {"left": '0px'};
                toAnim = 'left';
                break;
        }

        var tiles = $('.thing');
        var count = tiles.length;
        var self = this;

        for (var item = 0; item < tiles.length; item++) {
            tiles.eq(item).animate(animateToVal, 'slow', 'swing', function () {
                count--;

                if (!count) {
                    tiles.remove();
                    for (var tileIndex = 0; tileIndex < self.grid.length; tileIndex++) {
                        for (var item = 0; item < self.grid[tileIndex].length; item++) {
                            if (self.grid[tileIndex][item] != 0) {
                                var position = $(fieldsForTile[tileIndex][item]).position();

                                $('#playfield')
                                    .append($('<div class="thing t' + self.grid[tileIndex][item] + '" style="top: ' + position.top + 'px; left: '
                                        + position.left + 'px;"></div>'));
                            }
                        }

                        if (tileIndex == self.grid.length - 1) {
                            self.block = false;
                            self.randomTile();
                        }
                    }
                }
            });
        }

        if (!animateTo) {
            for (var tileIndex = 0; tileIndex < self.grid.length; tileIndex++) {
                for (var item = 0; item < self.grid[tileIndex].length; item++) {
                    if (self.grid[tileIndex][item] != 0) {
                        var position = $(fieldsForTile[tileIndex][item]).position();

                        $('#playfield')
                            .append($('<div class="thing t' + self.grid[tileIndex][item] + '" style="top: ' + position.top + 'px; left: '
                                + position.left + 'px;"></div>').hide().fadeIn(900));
                    }
                }
            }
        }
    };

    // New game / Restart game data
    MainGame.prototype.newGame = function () {
        $messageContainer
            .removeClass('message--success')
            .text('');

        this.grid = [
            [0, 0, 0, 0],
            [0, 2, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ];

        this.score = 0;
        $score.text(this.score);

        this.win = false;

        this.setupGame();
    };

    MainGame.prototype.eventsUDLF = function (event) {
        this.isWin();

        if (!this.win) {
            var lastScore = this.score;

            if (this.lose.up == 1 && this.lose.rigth == 1
                && this.lose.down == 1 && this.lose.left == 1) {
                $messageContainer
                    .addClass('message--success')
                    .text('Игра окончена! Начните заново!');
            } else {
                if (!this.block) {
                    this.block = true;

                    switch (event) {
                        case 'up':
                            this.mouseUp();

                            if (JSON.stringify(lastScore) == JSON.stringify(this.score)) {
                                this.lose.up = 1
                            } else {
                                this.clearLose();
                            }

                            this.setupGame('top');
                            break;
                        case 'right':
                            this.mouseRight(true);

                            if (JSON.stringify(lastScore) == JSON.stringify(this.score)) {
                                this.lose.rigth = 1
                            } else {
                                this.clearLose();
                            }

                            this.setupGame('right');
                            break;
                        case 'down':
                            this.mouseDown();

                            if (JSON.stringify(lastScore) == JSON.stringify(this.score)) {
                                this.lose.down = 1
                            } else {
                                this.clearLose();
                            }

                            this.setupGame('bottom');
                            break;
                        case 'left':
                            this.mouseLeft(true);

                            if (JSON.stringify(lastScore) == JSON.stringify(this.score)) {
                                this.lose.left = 1
                            } else {
                                this.clearLose();
                            }

                            this.setupGame('left');
                            break;
                    }
                } {
                    console.log(this.block)
                }
            }
        }
    };

    // Clear lose check parametres
    MainGame.prototype.clearLose = function () {
        this.lose.up = 0;
        this.lose.left = 0;
        this.lose.rigth = 0;
        this.lose.down = 0;
    };

    // Get all fieds for tile and convert to Matrix
    MainGame.prototype.fieldsToMatrix = function () {
        var fieldsForTile = $playfield.find('.back'),
            fieldsMatrix = [],
            i,
            k;

        for (i = 0, k = -1; i < fieldsForTile.length; i++) {
            if (i % this.size === 0) {
                k++;
                fieldsMatrix[k] = [];
            }

            fieldsMatrix[k].push(fieldsForTile.eq(i));
        }

        return fieldsMatrix;
    };

    // Generate random tile
    MainGame.prototype.randomTile = function () {
        var chance = Math.random() <= parseFloat(this.chance.max) / 100,
            type = chance ? 2 : 4,
            coordsEmpty = [];

        // Added empty fields to array
        for (var lineIndex = 0; lineIndex < this.grid.length; lineIndex++) {
            for (var columnIndex = 0; columnIndex < this.grid[lineIndex].length; columnIndex++) {
                if (this.grid[lineIndex][columnIndex] == 0) {
                    coordsEmpty.push({
                        x: lineIndex,
                        y: columnIndex
                    })
                }
            }
        }

        if (coordsEmpty.length !== 0) {

            // Get random empty field
            var randomCoord = Math.floor(Math.random() * coordsEmpty.length - 1),
                coord = coordsEmpty[randomCoord] || coordsEmpty[0],
                coordX = coord.x,
                coordY = coord.y;

            this.grid[coordX][coordY] = type;

            var position = $(this.fieldsForTile[coordX][coordY]).position();

            // Add new tile for randomCoord
            $playfield.append('<div class="thing t' + type + '" style="top: ' + position.top + 'px; left: '
                + position.left + 'px;"></div>');
        }
    };

    // Transpose matrix for tiles data
    MainGame.prototype.transposeMatrix = function (matrix) {
        return Object.keys(matrix[0]).map(
            function (ind) {
                return matrix.map(function (row) {
                    return row[ind];
                });
            }
        );
    };

    MainGame.prototype.mouseUp = function () {
        if (!this.win) {
            this.grid = this.transposeMatrix(this.grid);
            this.mouseLeft(false);
            this.grid = this.transposeMatrix(this.grid);
        } else {
            $messageContainer
                .addClass('message--success')
                .text('Вы победили! Начните новую игру.');
        }
    };

    MainGame.prototype.mouseRight = function () {

        for (var i = 0; i < this.grid.length; i++) {
            this.grid[i] = this.grid[i].reverse();
        }

        this.mouseLeft();

        for (var i = 0; i < this.grid.length; i++) {
            this.grid[i] = this.grid[i].reverse();
        }
    };

    MainGame.prototype.mouseDown = function () {
        if (!this.win) {
            this.grid = this.transposeMatrix(this.grid);
            this.mouseRight();
            this.grid = this.transposeMatrix(this.grid);
        } else {
            $messageContainer
                .addClass('message--success')
                .text('Вы победили! Начните новую игру.');
        }
    };

    MainGame.prototype.mouseLeft = function () {
        if (!this.win) {

            for (var lineIndex = 0; lineIndex < this.grid.length; lineIndex++) {
                var newItems = [];

                for (var columnIndex = 0; columnIndex < this.grid[lineIndex].length; columnIndex++) {
                    if (this.grid[lineIndex][columnIndex] !== 0) {
                        newItems.push(this.grid[lineIndex][columnIndex])
                    }
                }

                for (var k = 0; k < newItems.length; k++) {
                    if (k !== newItems.length) {
                        if (newItems[k] == newItems[k + 1]) {
                            if (!isNaN(newItems[k])) {
                                this.score += newItems[k] * 2;
                            }
                            newItems[k] *= 2;

                            for (var l = k + 1; l < newItems.length; l++) {
                                if (l !== newItems.length) {
                                    newItems[l] = newItems[l + 1]
                                }
                            }
                        }
                    }
                }

                var gr = [0, 0, 0, 0];

                for (var i = 0; i < newItems.length; i++) {
                    if (!isNaN(newItems[i]))
                        gr[i] = newItems[i];
                }

                this.grid[lineIndex] = gr;
            }

            $score
                .text(this.score);

            this.isWin();
        }
        else {
            $messageContainer
                .addClass('message--success')
                .text('Вы победили! Начните новую игру.');
        }
    };

    MainGame.prototype.isWin = function () {
        for (var lineIndex = 0; lineIndex < this.grid.length; lineIndex++) {
            for (var columnIndex = 0; columnIndex < this.grid[lineIndex].length; columnIndex++) {
                if (this.grid[lineIndex][columnIndex] === 2048) {
                    this.win = true;
                    $messageContainer
                        .addClass('message--success')
                        .text('Вы победили! Начните новую игру.');
                }
            }
        }
    };

//======================================================================= Game start ===============================

// Start new game
    var main = new MainGame();
    main.newGame();

    $btnNewGame.on('click', function () {
        main.newGame();
    });
})
;